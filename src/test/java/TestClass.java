import org.junit.Test;
import ro.FileReader.FileReader;
import ro.UtilsClass;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TestClass {

    @Test
    public void testFileReader() throws IOException {

        FileReader read = new FileReader();
        List<String> wordList = read.readInputFile(StandardCharsets.UTF_8);

        for (String line : wordList) {

            System.out.println(line);
        }
    }

    @Test
    public void testSortedSet() {
        List<String> list = new ArrayList<>();
        list.add("apa");
        list.add("liliac");
        list.add("buf");
        list.add("apa");

        Set<String> set = UtilsClass.convertToSet(list);
        for (String word : set) {
            System.out.println(word);
        }

    }

    @Test
    public void testRandom() {
        List<String> list = new ArrayList<>();
        list.add("apa");
        list.add("liliac");
        list.add("buf");
        list.add("papagal");
        list.add("papusa");
        list.add("paiscot");
        list.add("dinte");
        System.out.println(UtilsClass.randomWord(list));
    }
}
