package ro.FileReader;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

    public List<String> readInputFile(Charset cs) throws IOException {
        List<String> allwords=new ArrayList<>();
        File file = new File("C:\\CODE\\hangman\\src\\main\\resources\\dex.txt");
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, cs);
        BufferedReader br = new BufferedReader(isr);
        String line;
        while ((line = br.readLine()) != null) {
            allwords.add(line);
        }
        return allwords;
    }
}
